var _ = require('underscore');
var Model = require('./Model');
var ModelQuery = require('./ModelQuery');

class ModelRepo {

    constructor(db) {
        this.db = db;
        this.models = {};
    }

    register(model) {
        this.models[model.name] = model;
    }

    extend(name, func) {
        var model = this.get(name);
        var newModel = func(model);
        this.models[model.name] = newModel;
    }

    get(name) {
        return this.models[name];
    }

    has(name) {
        return _.has(this.models, name);
    }

    create(name, data) {
        var model = this.get(name);

        var inst = new model();
        inst.setRepo(this);

        if (data) {
            inst.fill(data);
        }

        return inst;
    }

    instanceOf(obj, name) {
        var model = this.get(name);
        return obj instanceof model;
    }

    inflate(name, data) {
        var model = this.get(name);

        var inst = model.deserialize(data);
        inst.setRepo(this);

        return inst;
    }

    query(name) {
        return new ModelQuery(this, name);
    }

}

module.exports = ModelRepo;