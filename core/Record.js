var RecordFlags = require('./RecordFlags');
var fs = require('fs');
var BSON = require('bson');
var Util = require('../helpers/Util');

class Record {

    constructor(file, pos) {
        this.file = file;

        this.length = 8;

        this.pos = pos;
        this.dataLength = 0;
        this.flags = 0;

        this.bson = new BSON();
    }

    load() {
        var buffer = this.file.readFile(this.pos, this.length);
        
        this.dataLength = buffer.readUInt32BE(0);
        this.flags = buffer.readUInt32BE(4);
    }

    save() {
        var buffer = Buffer.allocUnsafe(this.length);

        buffer.writeUInt32BE(this.dataLength, 0);
        buffer.writeUInt32BE(this.flags, 4);

        this.file.writeFile(buffer, this.pos);
    }

    setFlag(name, val) {
        if (val) {
            this.flags |= RecordFlags[name];
        } else {
            this.flags &= ~RecordFlags[name];
        }
    }

    hasFlag(name) {
        return this.flags & RecordFlags[name] > 0;
    }

    readDoc() {
        var buffer = this.file.readFile(this.pos + this.length, this.dataLength);

        var doc = this.bson.deserialize(buffer);
        doc = Util.unflatten(doc);

        return doc;
    }

    hasSpace(doc) {
        var size = this.bson.calculateObjectSize(doc);
        return this.dataLength >= size;
    }

    sizeTo(doc, padding) {
        var size = this.bson.calculateObjectSize(doc);
        this.dataLength = size + padding;
    }
    
    writeDoc(doc) {
        doc = Util.flatten(doc);
        var data = this.bson.serialize(doc);

        this.file.writeFile(data, this.pos + this.length);
    }

}

module.exports = Record;