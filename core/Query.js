var _ = require('underscore');

class Query {

    constructor(table) {
        this.table = table;

        this.parts = {};
        this.parts.where = {};
    }

    where(props) {
        _.extend(this.parts.where, props);
        return this;
    }

    findIds() {
        var ids = [];

        _.forEach(this.parts.where, (val, col) => {
            if (col == 'id') {
                ids.push([val]);
            } else {
                var index = this.table.getIndex(col);
                
                if (index) {
                    ids.push(index.lookup(val));
                }
            }
        }, this);

        return _.intersection.apply(null, ids);
    }

    find() {
        var ids = this.findIds();

        var docs = []

        _.each(ids, (id) => {
            var doc = this.table.select(id);

            if (doc) {
                docs.push(doc);
            }
        }, this);

        return docs;
    }

    first() {
        var docs = this.find();
        return docs.length ? docs[0] : null;
    }

    update(changes) {
        var ids = this.findIds();

        _.forEach(ids, (id) => {
            this.table.update(id, changes);
        }, this);
    }

    delete() {
        var ids = this.findIds();

        _.forEach(ids, (id) => {
            this.table.delete(id);
        }, this);
    }

}

module.exports = Query;