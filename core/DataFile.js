var fs = require('fs');
var _ = require('underscore');
var Record = require('./Record');

class DataFile {

    constructor(path) {
        this.path = path;

        this.fd = null;
        this.dataPadding = 128;

        if (!fs.existsSync(this.path)) {
            fs.writeFileSync(this.path, '');
        }

        this.dataPos = fs.statSync(this.path).size;
        this.fd = fs.openSync(this.path, 'r+');
    }

    readFile(pos, len) {
        var buffer = Buffer.allocUnsafe(len);
        fs.readSync(this.fd, buffer, 0, len, pos);
        return buffer;
    }

    writeFile(buffer, pos) {
        fs.writeSync(this.fd, buffer, 0, buffer.length, pos);
    }

    insert(doc) {
        var record = new Record(this, this.dataPos);

        record.sizeTo(doc, this.dataPadding);
        record.writeDoc(doc);
        record.save();

        var endPos = record.pos + record.length + record.dataLength;
        this.dataPos = Math.max(this.dataPos, endPos);

        return record;
    }

    delete(pos) {
        var record = this.select(pos);

        record.setFlag('DELETED', true);
        record.save();

        return record;
    }

    update(pos, doc) {
        var record = this.select(pos);

        if (record.hasSpace(doc)) {
            record.writeDoc(doc);
        } else {
            record.setFlag('DELETED', true);
            record.save();

            record = this.insert(doc);
        }

        return record;
    }

    select(pos) {
        var record = new Record(this, pos);
        record.load();
        
        if (record.hasFlag('DELETED')) {
            return null;
        } else {
            return record;
        }
    }

    close() {
        fs.closeSync(this.fd);
    }

}

module.exports = DataFile;