var Query = require('./Query');
var _ = require('underscore');

class ModelQuery extends Query {
    
    constructor(repo, name) {
        var inst = repo.create(name);

        super(inst.table);

        this.repo = repo;
        this.modelName = name;

        this.parts.with = [];
    }

    with(relations) {
        if (!_.isArray(relations)) {
            relations = [relations];
        }

        this.parts.with = _.union(this.parts.with, relations);
        
        return this;
    }

    find() {
        var docs = super.find();

        return _.map(docs, (doc) => {
            var model = this.repo.inflate(this.modelName, doc);

            if (this.parts.with.length) {
                model.load(this.parts.with);
            }

            return model;
        }, this);
    }

}

module.exports = ModelQuery;