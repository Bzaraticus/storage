var fs = require('fs');
var path = require('path');
var _ = require('underscore');
var Table = require('./Table');

class Database {

    constructor(baseDir) { 
        this.baseDir = baseDir;

        this.tables = [];
        this.models = {};
        this.queue = [];
    }

    // Core Stuff

    getPath(rel) {
        return path.join(this.baseDir, rel);
    }

    delete() {
        fs.unlinkSync(this.baseDir);
    }

    // Table Stuff

    table(name) {
        if (this.tables[name]) {
            return this.tables[name];
        }

        var table = new Table(this, name);

        this.tables[name] = table;

        return table;
    }

    // Model Stuff

    model(name, model) {
        if (model) {
            this.models[name] = model;
        } else {
            return this.models[name];
        }
    }
}

module.exports = Database;