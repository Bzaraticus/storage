var _ = require('underscore');

class Relation extends Function {

    constructor(parent, modelName, many) {
        super()

        this.parent = parent;
        this.modelName = modelName;
        this.many = many;
        this.ids = [];
        this.models = [];
        this.loaded = false;
        this.alwaysPopulate = false;

        return new Proxy(this, {
            apply: (target, thisArg, argList) => {
                return target.get();
            },
        });
    }

    populate(ids) {
        this.ids = ids;
        this.models = [];
        this.loaded = false;
    }

    load() {
        var models = _.map(this.ids, (id) => {
            return this.parent.repo.query(this.modelName).where({id: id}).first();
        }, this);

        this.models = _.filter(models);
        this.loaded = true;

        if (this.models.length == 0 && this.alwaysPopulate) {
            var model = this.parent.repo.create(this.modelName);
            this.add(model);
        }
    }

    ensureLoaded() {
        if (!this.loaded) {
            this.load();
        }
    }

    get() {
        this.ensureLoaded();

        if (this.many) {
            return this.models;
        } else {
            return this.models.length ? this.models[0] : null;
        }
    }

    add(model) {
        this.ensureLoaded();

        if (!model.exists()) {
            model.setRepo(this.parent.repo);
            model.save();
        }

        if (this.many) {
            this.models.push(model);
            this.ids.push(model.id);
        } else {
            this.models = [model];
            this.ids = [model.id];
        }
    
        this.parent.save();
    }

    remove(id) {
        this.ensureLoaded();

        this.ids = _.without(this.ids, id);

        this.models = _.filter(this.models, (model) => {
            return model.id != id;
        });

        this.parent.save();
    }

}

module.exports = Relation;