var _ = require('underscore');
var Relation = require('./Relation');

class Model {

    constructor() {
        this.repo = null;
        this.db = null;
        this.table = null;
        this.tableName = null;

        this.id = -1;
        this.fields = [];
        this.relations = [];

        this.extendFields({
            created: null,
            updated: null,
        });
    }

    setRepo(repo) {
        this.repo = repo;
        this.db = repo.db;
        this.table = repo.db.table(this.tableName);
    }

    hasOne(model) {
        return new Relation(this, model, false);
    }

    hasMany(model) {
        return new Relation(this, model, true);
    }

    extendFields(fields) {
        _.each(fields, (val, key) => {
            this[key] = val;

            if (val instanceof Relation) {
                this.relations.push(key);
            } else {
                this.fields.push(key);
            }
        }, this);
    }

    fill(data) {
        data = _.pick(data, this.fields);
        _.extend(this, data);
    }

    serialize() {
        var data = _.pick(this, this.fields); 

        _.each(this.relations, (relName) => {
            data[relName] = this[relName].ids;
        }, this);

        return data;
    }

    static deserialize(data) {
        var inst = new this();
        inst.id = data._id;
        inst.fill(data);

        _.each(inst.relations, (relName) => {
            if (_.has(data, relName)) {
                inst[relName].populate(data[relName]);
            }
        });

        return inst;
    }

    exists() {
        return this.id != -1;
    }

    touch() {
        this.updated = new Date();
    }

    save() {
        if (this.exists()) {
            this.updated = new Date();

            var data = _.pick(this, this.fields);
            data = this.serialize(data);

            this.table.update(this.id, data);
        } else {
            this.created = new Date();
            this.updated = new Date();
            
            var data = this.serialize();

            var doc = this.table.insert(data);
            this.id = doc._id;
        }

        return this;
    }

    update(data) {
        this.fill(data);
        this.save();
    }

    delete() {
        if (this.exists()) {
            this.table.delete(this.id);
            this.id = -1;
        }
    }

}

module.exports = Model;