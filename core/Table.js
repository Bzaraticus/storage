var fs = require('fs');
var path = require('path');
var _ = require('underscore');
var DataFile = require('./DataFile');
var mkdirp = require('mkdirp');
var Query = require('./Query');
var Util = require('../helpers/Util');
var IDIndex = require('../indexing/IDIndex');
var HashIndex = require('../indexing/HashIndex');

class Table {

    constructor(db, name) {
        this.db = db;
        this.name = name;
        
        this.baseDir = path.join(this.db.baseDir, name);

        if (!fs.existsSync(this.baseDir)) {
            mkdirp.sync(this.baseDir);
        }

        this.idIndex = IDIndex.load(this.getIndexPath('id'));
        this.indexes = {};
        this.dirtyIndexes = [];
        this.saveDirtyIndexes = _.debounce(this.saveDirtyIndexes, 943);

        this.store = new DataFile(this.getPath('store.dat'));
    }

    // Path functions

    getPath(rel) {
        return path.join(this.baseDir, rel);
    }

    getIndexPath(column) {
        return this.getPath(column.replace('.', '-') + '.index');
    }

    // Index functions

    shouldIndex(keypath) {
        for (var i = 0; i < keypath.length; i++) {
            if (keypath[i].substr(0, 1) == '_') {
                return false;
            }
        }

        return true;
    }

    saveDirtyIndexes() {
        for (var i = 0; i < this.dirtyIndexes.length; i++) {
            this.dirtyIndexes[i].save();
        }

        this.dirtyIndexes = [];
    }

    setIndexDirty(index) {
        this.dirtyIndexes = _.union(this.dirtyIndexes, [index]);
        this.saveDirtyIndexes();
    }

    getIndex(column) {
        var index = this.indexes[column];

        if (!index) {
            var filename = this.getIndexPath(column);
            index = HashIndex.load(filename);
            this.indexes[column] = index;
        }

        return index;
    }

    addToIndex(obj, id, keypath) {
        if (!keypath) {
            keypath = [];
        }

        if (_.isArray(obj)) {
            _.forEach(obj, (val) => {
                this.addToIndex(val, id, keypath);
            }, this);
        } else if (_.isObject(obj)) {
            _.forEach(obj, (val, key) => {
                var newpath = keypath.slice();
                newpath.push(key);

                this.addToIndex(val, id, newpath);
            }, this);
        } else if (obj && this.shouldIndex(keypath)) {
            var col = keypath.join('.');
            var index = this.getIndex(col);
            index.assign(obj, id);
            this.setIndexDirty(index);
        }
    }

    removeFromIndex(obj, id, keypath) {
        if (!keypath) {
            keypath = [];
        }

        if (_.isArray(obj)) {
            _.forEach(obj, (val) => {
                this.removeFromIndex(val, id, keypath);
            }, this);
        } else if (_.isObject(obj)) {
            _.forEach(obj, (val, key) => {
                var newpath = keypath.slice();
                newpath.push(key);

                this.removeFromIndex(val, id, newpath);
            }, this);
        } else if (obj && this.shouldIndex(keypath)) {
            var col = keypath.join('.');
            var index = this.getIndex(col);
            index.unassign(obj, id);
            this.setIndexDirty(index);
        }
    }

    // Record functions

    getRecord(id) {
        var pos = this.idIndex.lookup(id);

        if (pos.length) {
            var record = this.store.select(pos[0]);
            return record;
        } else {
            return null;
        }
    }

    insert(doc) {
        var id = this.idIndex.getNextKey();

        _.extend(doc, {
            _id: id,
        });

        var record = this.store.insert(doc);

        this.idIndex.assign(id, record.pos);
        this.setIndexDirty(this.idIndex);

        this.addToIndex(doc, id);

        return doc;
    }

    select(id) {
        var record = this.getRecord(id);
        return record ? record.readDoc() : null;
    }

    delete(id) {
        var record = this.getRecord(id);

        if (record) {
            var doc = record.readDoc();

            this.removeFromIndex(doc, id);

            this.store.delete(record.pos);
        }
    }

    update(id, changes) {
        var record = this.getRecord(id);

        if (record) {
            var doc = record.readDoc();
            var id = doc._id;

            var newDoc = _.clone(doc);
            _.extend(newDoc, changes);

            var newRecord = this.store.update(record.pos, newDoc);

            if (newRecord.pos != record.pos) {
                this.idIndex.unassign(id, record.pos);
                this.idIndex.assign(id, newRecord.pos);
                this.idIndex.save();
            }

            this.removeFromIndex(doc, id);
            this.addToIndex(newDoc, id);

            return newDoc;
        } else {
            return null;
        }
    }

    query() {
        return new Query(this);
    }
    
}

module.exports = Table;