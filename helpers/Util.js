var _ = require('underscore');

class Util {

    static setNested(obj, path, val) {
        path = _.isString(path) ? path.split('.') : path.slice();

        for (var i = 0; i < path.length; i++) {
            var key = path[i];

            if (i == path.length - 1) {
                obj[key] = val;
            } else {
                if (!_.has(obj, key)) {
                    obj[key] = {};
                }
                
                obj = obj[key];
            }
        }
    }

    static hasNested(obj, path) {
        path = _.isString(path) ? path.split('.') : path.slice();

        for (var i = 0; i < path.length; i++) {
            var key = path[i];

            if (i == path.length - 1) {
                return _.has(obj, key);
            } else if (_.has(obj, key)) {
                obj = obj[key];
            } else {
                return false;
            }
        }
    }

    static getNested(obj, path) {
        path = _.isString(path) ? path.split('.') : path.slice();

        for (var i = 0; i < path.length; i++) {
            var key = path[i];

            if (i == path.length - 1) {
                return obj[key];
            } else if (_.has(obj, key)) {
                obj = obj[key];
            } else {
                return undefined;
            }
        }
    }

    static flatten(obj, path) {
        path = path || [];

        var ret = {};

        _.each(obj, (val, key) => {
            var newpath = path.slice();
            newpath.push(key);

            if (_.isObject(val) && !_.isArray(val) && !(val instanceof Date)) {
                _.extend(ret, this.flatten(val, newpath));
            } else {
                ret[newpath.join('.')] = val;
            }
        }, this);

        return ret;
    }

    static unflatten(obj) {
        var ret = {};

        _.each(obj, (val, key) => {
            var path = key.split('.');

            this.setNested(ret, path, val);
        }, this);

        return ret;
    }

}

module.exports = Util;