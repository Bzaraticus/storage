var Storage = require('../../index');

class Ball extends Storage.Model {

    constructor() {
        super();

        this.tableName = 'balls';

        this.extendFields({
            size: 'small',
        });
    }

}

module.exports = Ball;