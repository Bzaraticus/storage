var Storage = require('../../index');

class Wolf extends Storage.Model {

    constructor() {
        super();

        this.tableName = 'wolves';

        this.extendFields({
            name: null,
            accounts: {},
            balls: this.hasMany('Ball'),
        });
    }

}

module.exports = Wolf;