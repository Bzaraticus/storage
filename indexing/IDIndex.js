var IndexFile = require('./IndexFile');

class IDIndex extends IndexFile {

    constructor(filename) {
        super(filename);

        this.type = 'id';

        this.data = [];
        this.dataFields = ['data'];
    }

    getNextKey() {
        return this.data.length + 1;
    }

    assign(key, val) {
        this.data[key - 1] = val;
    }

    unassign(key, val) {
        delete this.data[key - 1];
    }

    lookup(key) {
        var val = this.data[key - 1];
        return val == undefined ? [] : [val];
    }

}

module.exports = IDIndex;