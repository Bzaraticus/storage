var IndexFile = require('./IndexFile');
var _ = require('underscore');

class HashIndex extends IndexFile {

    constructor(filename) {
        super(filename);

        this.type = 'hash';
        
        this.data = {};
        this.dataFields = ['data'];
    }

    assign(key, val) {
        var keys = _.isArray(key) ? key : [key];

        for (var i = 0; i < keys.length; i++) {
            var key = keys[i];

            if (!_.has(this.data, key)) {
                this.data[key] = [];
            }

            if (this.data[key].indexOf(val) == -1) {
                this.data[key].push(val);
            }
        }
    }

    unassign(key, val) {
        var keys = _.isArray(key) ? key : [key];

        for (var i = 0; i < keys.length; i++) {
            var key = keys[i];

            if (_.has(this.data, key)) {
                this.data[key] = _.without(this.data[key], val);
            }
        }
    }

    lookup(key) {
        var val = this.data[key];
        return val == undefined ? [] : val;
    }

}

module.exports = HashIndex;