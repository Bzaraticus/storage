var fs = require('fs');
var BSON = require('bson');
var _ = require('underscore');

class IndexFile {

    constructor() {
        this.filename = null;

        this.settings = {};

        this.headerFields = ['settings'];
        this.dataFields = [];

        this.bson = new BSON();
    }

    static load(filename) {
        if (!fs.existsSync(filename)) {
            var index = new this();
            index.filename = filename;

            return index;
        } else {
            var file = fs.readFileSync(filename);

            var index = new this();
            index.filename = filename;

            var headerLen = file.readUInt32BE(0);
            var headerBuffer = file.slice(4, 4 + headerLen);
            var header = index.bson.deserialize(headerBuffer);
            _.extend(index, _.pick(header, index.headerFields));

            var dataBuffer = file.slice(4 + headerLen);
            var data = index.bson.deserialize(dataBuffer);
            _.extend(index, _.pick(data, index.dataFields));

            return index;
        }
    }

    save(filename) {
        if (filename) {
            this.filename = filename;
        }

        var headerFields = _.pick(this, this.headerFields);
        var header = this.bson.serialize(headerFields);

        var dataFields = _.pick(this, this.dataFields);
        var data = this.bson.serialize(dataFields);

        var buffer = Buffer.allocUnsafe(4 + header.length + data.length);
        buffer.writeUInt32BE(header.length, 0);
        header.copy(buffer, 4);
        data.copy(buffer, 4 + header.length);

        fs.writeFileSync(this.filename, buffer);
    }

    assign(key, val) {
        // Do Nothing
    }

    unassign(key, val) {
        // Do Nothing
    }

    lookup(key) {
        return [];
    }

}

module.exports = IndexFile;