var package = require('./package');

module.exports = {
    name: package.name,
    author: package.author,
    version: package.version,

    Database: require('./core/Database'),
    Model: require('./core/Model'),
    ModelRepo: require('./core/ModelRepo'),
};